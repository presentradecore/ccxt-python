# -*- coding: utf-8 -*-

# PLEASE DO NOT EDIT THIS FILE, IT IS GENERATED AND WILL BE OVERWRITTEN:
# https://github.com/ccxt/ccxt/blob/master/CONTRIBUTING.md#how-to-contribute-code

from ccxt.liqui import liqui
import hashlib
import time


class dsx (liqui):

    def describe(self):
        return self.deep_extend(super(dsx, self).describe(), {
            'id': 'dsx',
            'name': 'DSX',
            'countries': 'UK',
            'rateLimit': 1500,
            'has': {
                'CORS': False,
                'fetchOrder': True,
                'fetchOrders': True,
                'fetchOpenOrders': True,
                'fetchClosedOrders': True,
                'fetchTickers': True,
                'fetchMyTrades': True,
            },
            'urls': {
                'logo': 'https://user-images.githubusercontent.com/1294454/27990275-1413158a-645a-11e7-931c-94717f7510e3.jpg',
                'api': {
                    'public': 'https://dsx.uk/mapi',  # market data
                    'private': 'https://dsx.uk/tapi',  # trading
                    'dwapi': 'https://dsx.uk/dwapi',  # deposit/withdraw
                },
                'www': 'https://dsx.uk',
                'doc': [
                    'https://api.dsx.uk',
                    'https://dsx.uk/api_docs/public',
                    'https://dsx.uk/api_docs/private',
                    '',
                ],
            },
            'api': {
                # market data(public)
                'public': {
                    'get': [
                        'barsFromMoment/{id}/{period}/{start}',  # empty reply :\
                        'depth/{pair}',
                        'info',
                        'lastBars/{id}/{period}/{amount}',  # period is(m, h or d)
                        'periodBars/{id}/{period}/{start}/{end}',
                        'ticker/{pair}',
                        'trades/{pair}',
                    ],
                },
                # trading(private)
                'private': {
                    'post': [
                        '/info/account',
                        '/history/trades',
                        '/orders',
                        '/order/new',
                        '/order/cancel'
                    ],
                },
                'fees': {
                    'trading': {
                        'taker': 0.0025 / 100, # Special Fees until 06.08.2018
                        'maker': 0.0025 / 100, # Special Fees until 06.08.2018
                    },
                },
                # deposit / withdraw(private)
                'dwapi': {
                    'post': [
                        'getCryptoDepositAddress',
                        'cryptoWithdraw',
                        'fiatWithdraw',
                        'getTransactionStatus',
                        'getTransactions',
                    ],
                },
            },
        })

    def get_fee(self, pair):
        ''' 0 fees until 06.08.18 '''
        return 0.0

    def fetch_markets(self):
        response = self.publicGetInfo()
        markets = response['pairs']
        keys = list(markets.keys())
        result = []
        for p in range(0, len(keys)):
            id = keys[p]
            market = markets[id]
            base = market['base_currency']
            quote = market['quoted_currency']
            symbol = base + '/' + quote
            precision = {
                'amount': self.safe_integer(market, 'amount_decimal_places'),
                'price': self.safe_integer(market, 'decimal_places'),
            }
            amountLimits = {
                'min': self.safe_float(market, 'min_amount'),
                'max': self.safe_float(market, 'max_amount'),
            }
            priceLimits = {
                'min': self.safe_float(market, 'min_price'),
                'max': self.safe_float(market, 'max_price'),
            }
            costLimits = {
                'min': self.safe_float(market, 'min_total'),
            }
            limits = {
                'amount': amountLimits,
                'price': priceLimits,
                'cost': costLimits,
            }
            hidden = self.safe_integer(market, 'hidden')
            active = (hidden == 0)
            result.append({
                'id': id,
                'symbol': symbol,
                'base': base,
                'quote': quote,
                'active': active,
                'taker': None,
                'maker': None,
                'lot': amountLimits['min'],
                'precision': precision,
                'limits': limits,
                'info': market,
            })

        return result

    def fetch_balance(self, params={}):
        self.load_markets()
        response = self.privatePostInfoAccount()
        balances = response['return']
        result = {'info': balances}
        funds = balances['funds']
        currencies = list(funds.keys())
        for c in range(0, len(currencies)):
            currency = currencies[c]
            uppercase = currency.upper()
            uppercase = self.common_currency_code(uppercase)
            account = {
                'free': funds[currency]['available'],
                'used': funds[currency]['total'] - funds[currency]['available'],
                'total': funds[currency]['total'],
            }
            result[uppercase] = account
        return self.parse_balance(result)

    def fetch_my_trades(self, symbol=None, since=None, limit=None, params={}):
        self.load_markets()
        market = None
        request = {
            # 'from': 123456789,  # trade ID, from which the display starts numerical 0(test result: liqui ignores self field)
            # 'count': 1000,  # the number of trades for display numerical, default = 1000
            # 'from_id': trade ID, from which the display starts numerical 0
            # 'end_id': trade ID on which the display ends numerical ∞
            # 'order': 'ASC',  # sorting, default = DESC(test result: liqui ignores self field, most recent trade always goes last)
            # 'since': 1234567890,  # UTC start time, default = 0(test result: liqui ignores self field)
            # 'end': 1234567890,  # UTC end time, default = ∞(test result: liqui ignores self field)
            # 'pair': 'eth_btc',  # default = all markets
        }
        if symbol is not None:
            market = self.market(symbol)
            request['pair'] = market['id']
        if limit is not None:
            request['count'] = int(limit)
        if since is not None:
            request['since'] = int(since / 1000)
        response = self.privatePostHistoryTrades(self.extend(request, params))
        trades = []
        if 'return' in response:
            trades = response['return']

        parsed_trades = self.parse_trades(trades, market, since, limit)
        return parsed_trades

    def parse_ticker(self, ticker, market=None):
        timestamp = ticker['updated'] * 1000
        symbol = None
        if market:
            symbol = market['symbol']
        average = self.safe_float(ticker, 'avg')
        if average is not None:
            if average > 0:
                average = 1 / average
        last = self.safe_float(ticker, 'last')
        return {
            'symbol': symbol,
            'timestamp': timestamp,
            'datetime': self.iso8601(timestamp),
            'high': self.safe_float(ticker, 'high'),
            'low': self.safe_float(ticker, 'low'),
            'bid': self.safe_float(ticker, 'buy'),
            'bidVolume': None,
            'ask': self.safe_float(ticker, 'sell'),
            'askVolume': None,
            'vwap': None,
            'open': None,
            'close': last,
            'last': last,
            'previousClose': None,
            'change': None,
            'percentage': None,
            'average': average,
            'baseVolume': self.safe_float(ticker, 'vol'),
            'quoteVolume': self.safe_float(ticker, 'vol_cur'),
            'info': ticker,
        }

    def get_order_id_key(self):
        return 'orderId'

    def sign_body_with_secret(self, body):
        return self.decode(self.hmac(self.encode(body), self.encode(self.secret), hashlib.sha512, 'base64'))

    def get_version_string(self):
        return ''  # they don't prepend version number to public URLs as other BTC-e clones do

    def create_order(self, symbol, type, side, amount, price=None, params={}):
        if type == 'market':
            raise ExchangeError(self.id + ' allows limit orders only')
        self.load_markets()
        market = self.market(symbol)
        request = {
            'pair': market['id'],
            'type': side,
            'volume': self.amount_to_precision(symbol, amount),
            'orderType': 'limit',
            'rate': self.price_to_precision(symbol, price),
        }
        response = self.privatePostOrderNew(self.extend(request, params))
        id = self.safe_string(response['return'], self.get_order_id_key())
        timestamp = self.milliseconds()
        price = float(price)
        amount = float(amount)
        status = 'open'
        if id == '0':
            id = self.safe_string(response['return'], 'init_order_id')
            status = 'closed'
        filled = self.safe_float(response['return'], 'received', 0.0)
        remaining = self.safe_float(response['return'], 'remains', amount)
        order = {
            'id': id,
            'timestamp': timestamp,
            'datetime': self.iso8601(timestamp),
            'status': status,
            'symbol': symbol,
            'type': type,
            'side': side,
            'price': price,
            'cost': price * filled,
            'amount': remaining,
            'start_amount': remaining,
            'remaining': remaining,
            'filled': filled,
            'fee': None,
            # 'trades': self.parse_trades(order['trades'], market),
        }
        self.orders[id] = order
        return self.extend({'info': response}, order)

    def cancel_order(self, id, symbol=None, params={}):
        self.load_markets()
        request = {}
        idKey = self.get_order_id_key()
        request[idKey] = id
        response = self.privatePostOrderCancel(self.extend(request, params))
        if id in self.orders:
            self.orders[id]['status'] = 'canceled'

        return response

    def fetch_open_orders(self, symbol=None, since=None, limit=None, params={}):
        if 'fetchOrdersRequiresSymbol' in self.options:
            if self.options['fetchOrdersRequiresSymbol']:
                if symbol is None:
                    raise ExchangeError(self.id + ' fetchOrders requires a symbol argument')
        self.load_markets()
        request = {}
        market = None
        if symbol is not None:
            market = self.market(symbol)
            request['pair'] = market['id']
        response = self.privatePostOrders(self.extend(request, params))

        # liqui etc can only return 'open' orders(i.e. no way to fetch 'closed' orders)
        open_orders = []
        if 'return' in response:
            open_orders = self.parse_orders(response['return'], market)

        return open_orders

    def parse_orders(self, orders, market=None, since=None, limit=None):
        ids = list(orders.keys())
        result = []
        for i in range(0, len(ids)):
            id = ids[i]
            order = orders[id]
            extended = self.extend(order, {'id': id})
            result.append(self.parse_order(extended, market))
        return result

    def parse_order(self, order, market=None):
        id = str(order['id'])
        status = self.safe_float(order, 'status')
        timestamp = int(order['timestampCreated']) * 1000

        # Set symbol
        symbol = None
        if not market:
            market = self.markets_by_id[order['pair']]
        if market:
            symbol = market['symbol']
        remaining = None

        # Set amount
        try:
            amount = order['amount']
        except:
            amount = order['volume']

        price = self.safe_float(order, 'rate')
        cost = None

        try:
            remaining = order['remainingVolume']
        except:
            remaining = 0.0
        filled = amount - remaining

        fee = None
        result = {
            'info': order,
            'id': id,
            'symbol': symbol,
            'timestamp': timestamp,
            'datetime': self.iso8601(timestamp),
            'type': 'limit',
            'side': order['type'],
            'price': price,
            'cost': cost,
            'amount': amount,
            'remaining': remaining,
            'filled': filled,
            'status': status,
            'fee': fee,
        }
        return result

    def parse_trade(self, trade, market=None):
        timestamp = int(trade['timestamp']) * 1000
        side = trade['type']

        try:
            price = trade['rate']
        except:
            price = trade['price']

        id = self.safe_string(trade, 'orderId')

        if 'pair' in trade:
            marketId = trade['pair']
            market = self.markets_by_id[marketId]
        symbol = None
        if market:
            symbol = market['symbol']

        try:
            amount = trade['amount']
        except:
            amount = trade['volume']

        type = 'limit'  # all trades are still limit trades
        order = self.safe_string(trade, self.get_order_id_key())

        return {
            'id': id,
            'order': order,
            'timestamp': timestamp,
            'datetime': self.iso8601(timestamp),
            'symbol': symbol,
            'type': type,
            'side': side,
            'price': price,
            'amount': amount,
            'fee': 0.0,
            'info': trade,
        }

    def nonce(self):
        return self.milliseconds()

    def sign(self, path, api='public', method='GET', params={}, headers=None, body=None):
          url = self.urls['api'][api]
          query = self.omit(params, self.extract_params(path))
          if api == 'private':
              self.check_required_credentials()
              nonce = self.nonce()
              body = self.urlencode(self.extend({
                  'nonce': nonce,
              }, query))
              url += '/v2' + path
              signature = self.sign_body_with_secret(body)
              headers = {
                  'Content-Type': 'application/x-www-form-urlencoded',
                  'Key': self.apiKey,
                  'Sign': signature,
              }
          elif api == 'public':
              url += self.get_version_string() + '/' + self.implode_params(path, params)
              if query:
                  url += '?' + self.urlencode(query)
          else:
              url += '/' + self.implode_params(path, params)
              if method == 'GET':
                  if query:
                      url += '?' + self.urlencode(query)
              else:
                  if query:
                      body = self.json(query)
                      headers = {
                          'Content-Type': 'application/json',
                      }

          result = {'url': url, 'method': method, 'body': body, 'headers': headers}
          return result
